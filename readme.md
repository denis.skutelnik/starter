![image](logo_cmd.png) 
 
# Starter 
 
Creates python project with:
- resources directory (including logo)
- readme.md
- main.py
- local git repository (including initial commit)
