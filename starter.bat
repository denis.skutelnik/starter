@echo off
call :setESC

:folder_request
echo.
set /p name=%ESC%[93mPlease enter project name: 
echo %ESC%[0m 
if exist ..\%name%\ (
  echo %ESC%[91mProject %name% already exists. Please choose other name%ESC%[0m
  pause
  exit /B
)
choice /M "Do you want to create project in automatic mode?"
if %errorlevel% EQU 2 set /a auto=0
if %errorlevel% EQU 1 set /a auto=1

:folder_create
  md ..\%name%\
  cd ..\%name%\
  echo Project folder created  
)

:res_request
if /I %auto% EQU 1 goto res_create
choice /M "Should I create resources directory?"
if %errorlevel% EQU 2 goto readme_request
if %errorlevel% EQU 1 goto res_create

:res_create
md .\resources\
copy ..\starter\templates\logo_pyt.png .\resources\logo_pyt.png
echo Resources directory with logo created

:readme_request
if /I %auto% EQU 1 goto readme_create
choice /M "Should I create readme.md?"
if %errorlevel% EQU 2 goto py_request
if %errorlevel% EQU 1 goto readme_create

:readme_create
if exist .\resources\logo_pyt.png echo ![image](resources/logo_pyt.png)>readme.md & echo. >>readme.md
echo # Project: %name% >>readme.md
echo. >>readme.md
echo (add project description here) >>readme.md
echo readme.md created

:main_create
echo print("Hello World!") >main.py
echo main.py created

:py_request
if not exist .\main.py goto main_create
if /I %auto% EQU 1 goto venv_create
choice /M "Should I create venv?"
if %errorlevel% EQU 2 goto git_request
if %errorlevel% EQU 1 goto venv_create

:venv_create
python -m venv venv
call .\venv\Scripts\activate.bat
.\venv\Scripts\python.exe -m pip install --upgrade pip

:git_request
if /I %auto% EQU 1 goto git_create
choice /M "Should I create git repository?"
if %errorlevel% EQU 2 goto end
if %errorlevel% EQU 1 goto git_create

:git_create
git init
if exist ..\starter\templates\gitignore (
  copy ..\starter\templates\gitignore .gitignore
) else (
  curl -Lo .gitignore https://github.com/github/gitignore/raw/main/Python.gitignore
)
git config --local user.name "starter.bat"
git config --local user.email "don't.write@to.me"
git add .
git commit -m "Initial commit"
git config --local user.name "Denys Skutelnyk"
git config --local user.email "denis.skutelnik@gmail.com"

:end
echo %ESC%[92mAll processed fine!%ESC%[0m
pause
exit

:setESC
for /F "tokens=1,2 delims=#" %%a in ('"prompt #$H#$E# & echo on & for %%b in (1) do rem"') do set ESC=%%b